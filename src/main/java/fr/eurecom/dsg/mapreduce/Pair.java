package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Pair extends Configured implements Tool {


    public static class PairMapper
            extends Mapper<LongWritable, Text, TextPair, LongWritable> {

        @Override
        protected void map(LongWritable key,
                           Text value,
                           Context context) throws IOException, InterruptedException {
            StringTokenizer lineTokenizer = new StringTokenizer(value.toString(), ".?!");
            while (lineTokenizer.hasMoreElements()) {
                String line = lineTokenizer.nextToken();
                line = line.trim();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                List<String> wordList = new ArrayList<String>();
                while (tokenizer.hasMoreElements()) {
                    String word = tokenizer.nextToken();
                    word = word.toLowerCase();
                    wordList.add(word);
                }
                LongWritable one = new LongWritable(1);
                for (String word : wordList) {
                    for (String pairWord : wordList) {
                        if (!word.equals(pairWord)) {
                            TextPair pair = new TextPair(word, pairWord);
                            context.write(pair, one);
                        }
                    }
                }
            }
        }
    }
    public static class PairReducer
                extends Reducer<TextPair, // TODO: change Object to input key type
                LongWritable, // TODO: change Object to input value type
                TextPair, // TODO: change Object to output key type
                LongWritable> { // TODO: change Object to output value type

        @Override
        protected void reduce(TextPair pair,
                                  Iterable<LongWritable> values,
                                  Context context) throws IOException, InterruptedException {

            int sum = 0;
            for (LongWritable currentInt : values) {
                    sum += currentInt.get();
            }
            context.write(pair, new LongWritable(sum));
        }
    }

        private final int numReducers;
        private final Path inputPath;
        private final Path outputDir;

        public Pair(String[] args) {
            if (args.length != 3) {
                System.out.println("Usage: Pair <num_reducers> <input_path> <output_path>");
                System.exit(0);
            }
            this.numReducers = Integer.parseInt(args[0]);
            this.inputPath = new Path(args[1]);
            this.outputDir = new Path(args[2]);
        }


        @Override
        public int run(String[] args) throws Exception {

            Configuration conf = this.getConf();
            Job job = new Job(conf);  // TODO: define new job instead of null using conf e setting a name

            job.setJobName("Pair");
            job.setInputFormatClass(TextInputFormat.class);

            job.setMapperClass(PairMapper.class);
            job.setMapOutputKeyClass(TextPair.class);
            job.setMapOutputValueClass(LongWritable.class);


            job.setReducerClass(PairReducer.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(LongWritable.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            TextInputFormat.setInputPaths(job, inputPath);
            TextOutputFormat.setOutputPath(job, outputDir);
            job.setNumReduceTasks(numReducers);

            job.setJarByClass(Pair.class);

            return job.waitForCompletion(true) ? 0 : 1;
        }

        public static void main(String[] args) throws Exception {
            int res = ToolRunner.run(new Configuration(), new Pair(args), args);
            System.exit(res);
        }
    }
