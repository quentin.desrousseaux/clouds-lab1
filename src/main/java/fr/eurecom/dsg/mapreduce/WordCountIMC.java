package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 *
 */
public class WordCountIMC extends Configured implements Tool {

    private int numReducers;
    private Path inputPath;
    private Path outputDir;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = new Job(conf);// TODO: define new job instead of null using conf e setting
        job.setJobName("Word Count IMC");// a name

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputKeyClass(Text.class);

        job.setMapperClass(WCIMCMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setReducerClass(WCIMCReducer.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        TextInputFormat.setInputPaths(job, inputPath);//       inputPath
        TextOutputFormat.setOutputPath(job, outputDir);//       outputPath
        job.setNumReduceTasks(numReducers);
        job.setJarByClass(WordCount.class);

        return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
    }

    public WordCountIMC (String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: WordCountIMC <num_reducers> <input_path> <output_path>");
            System.exit(0);
        }
        this.numReducers = Integer.parseInt(args[0]);
        this.inputPath = new Path(args[1]);
        this.outputDir = new Path(args[2]);
    }

    public static void main(String args[]) throws Exception {
        int res = ToolRunner.run(new Configuration(), new WordCountIMC(args), args);
        System.exit(res);
    }
}

class WCIMCMapper extends Mapper<LongWritable, // TODO: change Object to input key
        // type
        Text, // TODO: change Object to input value type
        Text, // TODO: change Object to output key type
        IntWritable> { // TODO: change Object to output value type
    private final Map< String , Integer> counts = new HashMap<String, Integer>();

    @Override
    protected void map(LongWritable key, // TODO: change Object to input key type
                       Text value, // TODO: change Object to input value type
                       Context context) throws IOException, InterruptedException {

       for (String word : value.toString().split("\\s+")) {
           word = word.toLowerCase();
           if (!counts.containsKey(word)) {
               counts.put(word, 1);// * TODO: implement the map method (use context.write to emit results). Use
           } else {
               counts. put(word, counts.get(word)+1);
           }
       }
    }

    @Override
    protected void cleanup(Context context)
            throws IOException, InterruptedException {
        super.cleanup(context);

        for (String text: counts.keySet()) {
            int wordCount = counts.get(text);
            context.write(new Text(text), new IntWritable(wordCount));
        }
    }
}

class WCIMCReducer extends Reducer<Text, // TODO: change Object to input key
        // type
        IntWritable, // TODO: change Object to input value type
        Text, // TODO: change Object to output key type
        LongWritable> { // TODO: change Object to output value type

    @Override
    protected void reduce(Text key, // TODO: change Object to input key type
                          Iterable<IntWritable> values, // TODO: change Object to input value type
                          Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable currentInt: values) {
            sum += currentInt.get();
        }
        context.write(key, new LongWritable(sum));
        // TODO: implement the reduce method (use context.write to emit results)
    }
}